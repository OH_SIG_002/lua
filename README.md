# lua

## 简介
> Lua是一种功能强大、高效、轻量级、可嵌入的脚本语言。
> 支持过程编程、面向对象编程、函数编程、数据驱动编程和数据描述。

![](screenshot/result.png)

## 下载安装
直接在OpenHarmony-SIG仓中搜索lua并下载。

## 使用说明
以OpenHarmony 3.1 Beta的rk3568版本为例

1. 将下载的lua库代码存在以下路径：./third_party/lua

2. 将libogg库一起放入third_party目录

3. 修改添加依赖的编译脚本，路径：/developtools/bytrace_standard/ohos.build

```

{
  "subsystem": "developtools",
  "parts": {
    "bytrace_standard": {
      "module_list": [
        "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
        "//developtools/bytrace_standard/bin:bytrace_target",
        "//developtools/bytrace_standard/bin:bytrace.cfg",
        "//developtools/bytrace_standard/interfaces/kits/js/napi:bytrace",
        "//third_party/lua:lua",
        "//third_party/lua:lua_exe",
        "//third_party/lua:luac_exe"
      ],
      "inner_kits": [
        {
          "type": "so",
          "name": "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
          "header": {
            "header_files": [
              "bytrace.h"
            ],
            "header_base": "//developtools/bytrace_standard/interfaces/innerkits/native/include"
          }
        }
      ],
      "test_list": [
        "//developtools/bytrace_standard/bin/test:unittest"
      ]
    }
  }
}

```

4. 编译：./build.sh --product-name rk3568 --ccache

5. 生成库文件和一些可执行测试文件，路径：out/rk3568/developtools/profiler

## 接口说明

元表和元方法：
__add：添加（+）操作。

__sub：减法（-）运算。

__div：除法（/）操作。

__mod：模（%）运算。

__pow：指数运算（^）。

__unm：否定（一元）运算。

__idiv：楼层划分（//）操作。

__band：按位AND（&）操作。

__bor：按位OR（|）操作。

__bxor：按位异或（二进制~）操作。

__bnot：按位NOT（一元~）操作。

__shl：按位左移（<<）操作。

__shr：按位右移（>>）操作。

__concat：串联（…）活动行为类似于加法运算。

__len：长度（#）操作。

__eq：相等（=）操作。

__lt：小于（<）操作。

__le：较小相等（<=）操作。

__index：索引访问操作表[键]。

__newindex：索引分配表[键]=值。

__call：调用操作func（args）。

详细接口说明参考[Lua 5.4 参考手册](https://gitee.com/openharmony-sig/lua/blob/master/doc/contents.html)


## 约束与限制

在下述版本验证通过：

OpenHarmony SDK版本：API version 8版本

## 目录结构
````
|---- lua
|     |---- doc                 #参考手册及说明文件
|     |---- src
|           |---- lapi.c        #Lua api
|           |---- lauxlib.c     #构建Lua库的辅助功能
|           |---- lbaselib.c    #基础库
|           |---- lcode.c       #Lua代码生成器
|           |---- lcorolib.c    #协同程序库
|           |---- lctype.c      #Lua的“ctype”函数
|           |---- ldebug.c      #调试接口
|           |---- ldo.c         #Lua的堆栈和调用结构
|           |---- ldump.c       #保存预编译的Lua块
|           |---- lfunc.c       #操纵原型和闭包的辅助函数
|           |---- linit.c       #lua库的初始化
|           |---- liolib.c      #标准输入/输出（和系统）库
|           |---- lmathlib.c    #标准数学库
|           |---- lmem.c        #内存管理器接口
|           |---- loadlib.c     #Lua的动态库加载器
|           |---- lobject.c     #Lua对象上的一些泛型函数
|           |---- lopcodes.c    #Lua虚拟机的操作码
|           |---- loslib.c      #标准操作系统库
|           |---- lparser.c     #Lua解析器
|           |---- lstate.c      #全局状态
|           |---- lstring.c     #字符串表（保留Lua处理的所有字符串）
|           |---- lstrlib.c     #字符串操作和模式匹配的标准库
|           |---- ltable.c      #Lua表（哈希）
|           |---- ltablib.c     #用于表操作的库
|           |---- ltm.c         #标记方法
|           |---- lua.c         #Lua独立解释器
|           |---- luac.c        #Lua编译器
|           |---- lundump.c     #加载预编译的Lua块
|           |---- lutf8lib.c    #UTF-8操作的标准库
|           |---- lvm.c         #Lua虚拟机
|           |---- lzio.c        #缓冲流
|     |---- README.md           #安装使用方法
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/lua/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/lua/pulls) 。

## 开源协议
本项目基于 [MIT License](https://gitee.com/openharmony-sig/lua/blob/master/doc/readme.html) ，请自由地享受和参与开源。